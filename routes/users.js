const express = require('express'),
 router = express.Router(),
 usuario = require('../models/usuario'),
 funciones = require('../functions/funciones'),
 sendMail = require('../functions/send_email'),
 encrip = require('../functions/encrip');

//var mongoose = require('mongoose');  

/* GET users listing. */
router.get('/index', (req, res, next) => {
    
    res.render('dashboard/login');
});

router.post('/login', (req, res, next) => {
    
    let email = req.body.email;
    let password = req.body.password;
    let pass = encrip.encriptar(password);
    if(funciones.validarConexionMongoDB()===false){
        req.flash('error','Se ha perdido conexion con el servidor de la base de datos.');
        res.redirect('/users/index');
    }else{
        usuario.findOne({email: email, password: pass}, (err,user) => {
            if(err){
                req.flash('error',`Error interno del sistema contacte al administrador.`);
                res.redirect('/users/index');
            }else{
                if(user != null){
                    console.log(user);
                    req.session.user_id = user._id;
                    req.flash('info',`Hola ${user.nombre} bienvenido al sistema`);
                    res.redirect('/dashboard');
                }else{
                    req.flash('error',`No existe usuario registrado con las credenciales que esta intentando acceder.`);
                    res.redirect('/users/index');
                }
            }
        });
    }
});

router.get('/logout', (req, res, next) => {
    
    req.session = null;
    res.redirect('/users/index');
})

router.post('/registro', (req, res, next) => {

    let nombre = req.body.full_name;
    let direccion = req.body.address;
    let ciudad = req.body.city;
    let genero = req.body.genero;
    let email = req.body.email;
    let password = req.body.password;
    let pass = encrip.encriptar(password); 

    let user = new usuario({
        nombre: nombre,
        direccion : direccion,
        ciudad: ciudad,
        genero: genero,
        email: email,
        password: pass
    })
    user.save()
        .then(msg => {
            console.log(`Se guardo correctamente ${msg}`);
            req.flash('success','Usuario registrado con exito, proceda a iniciar sesion para ingresar al dashboard');
            res.redirect('/users/index');
        })
        .catch(error => {
            console.log(`Hay un error al guardar el usuario: ${error.errmsg}`);
            req.flash('error',error.errmsg);
            res.redirect('/users/index');
        });
});

router.post('/restablecer', (req, res, next) => {
    let email = req.body.email;
    usuario.findOne({email: email}, (err,user) => {
        if(err){
            req.flash('error',`Error interno del sistema contacte al administrador.`);
            res.redirect("/users/index");
        }else{
            if(user != null){
                sendMail.envio('Recuperar contraseña',`Hola su contraseña de acceso es: ${encrip.desencriptar(user.password)}`,user.email)
                    .then(msg => console.log(msg));
                req.flash('info',`Se ha enviado a su correo sus credenciales de acceso.`);
                res.redirect("/users/index");
            }else{
                req.flash('error',`No existe usuario registrado con las credenciales que esta intentando acceder.`);
                res.redirect("/users/index");
            }
        }
    });
});

module.exports = router;