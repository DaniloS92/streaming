const usuario = require('../models/usuario');

module.exports = (req, res, next) => {
    if(!req.session.user_id){
        res.redirect('/users/index');//llega una peticion si no esta registrado envia a iniciar sesion
    }else{
        usuario.findById(req.session.user_id)
            .then(user =>{
                res.locals.user = user;
                next();
            })
            .catch(err => {
                console.log(err);
                redirect('users/index');
            })
    }
}