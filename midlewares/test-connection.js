module.exports = (req, res, next) => {
    var mongoose = require('mongoose');  
    if(mongoose.connection.readyState != 1){
        req.flash('Error','Se ha perdido conexion con el servidor de la base de datos.');
        next();
    }
}