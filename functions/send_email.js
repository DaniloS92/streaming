'use strict';
const nodemailer = require('nodemailer');

// create reusable transporter object using the default SMTP transport
let transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'notificaciones.daylisoft@gmail.com',
        pass: 'daylisoft123'
    }
});

function envio(subject, text, destinatario){
    return new Promise((resolve, reject) => {
        // setup email data with unicode symbols
        let mailOptions = {
            from: '"Notificacion" <notificaciones.daylisoft@gmail.com>', // sender address
            to: destinatario, // list of receivers
            subject: `${subject} ✔`, // Subject line
            text: text, // plain text body
            html: `<b>${text}</b>` // html body
        };

        // send mail with defined transport object
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                reject(error);
            }
            resolve('Message %s sent: %s', info.messageId, info.response);
        });
    })
    
}

module.exports.envio = envio;