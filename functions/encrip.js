const crypto = require('crypto');
let algorithm = 'aes256'; // or any other algorithm supported by OpenSSL
let key = '00000000000000000000000000000000';
let iv = '0000000000000000';

exports.encriptar = (text) => {
    let cipher = crypto.createCipheriv(algorithm, key, iv);
    let encrypted = cipher.update(text, 'utf8', 'hex') + cipher.final('hex');
    return encrypted;
}
exports.desencriptar = (encrypted) => {
    let decipher = crypto.createDecipheriv(algorithm, key, iv);
    let dec = decipher.update(encrypted,'hex','utf8') + decipher.final('utf8');
    console.log('decrypted', dec, dec.length)
    return dec;
}

exports.encriptarSiempre = (user, pass) => {
    let promise = new Promise((resolve,reject) => {
        // usamos el metodo CreateHmac y le pasamos el parametro user y actualizamos el hash con la password
        try {
            let hmac = crypto.createHmac('sha1', user).update(pass).digest('hex');
            resolve(hmac);
        } catch (error) {
            reject(error);
        }
    });
    return promise;
}