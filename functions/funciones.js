var mongoose = require('mongoose');  

function validarConexionMongoDB(){

        try {
            if(mongoose.connection.readyState === 0){
                console.log('no hay conexion');
                return false;
            }else{
                console.log('si hay conexion');
                return true;
            }
        } catch (error) {
            console.log(error);
        }

}

module.exports.validarConexionMongoDB = validarConexionMongoDB;