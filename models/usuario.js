const mongoose = require('./db.js');  
const Schema = mongoose.Schema;

var userSchema = new Schema({  
    nombre: String,
    direccion: String,
    ciudad: String,
    genero: String,
    email: { type:String, unique: true, required: true},
    password: { type:String, required: true}
});

module.exports = mongoose.model('Usuario', userSchema);