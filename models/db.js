const mongoose = require('mongoose');  
var mongoUrl = "mongodb://localhost/stream"

mongoose.Promise = Promise;


var reintentarConexion = function() {
  return mongoose.connect(mongoUrl, function(err) {
    if (err) {
      console.error('Error de conexion: no esta inicializado el servidor de mongoDB - reintentando en 5 seg');
      setTimeout(reintentarConexion, 5000);
    }
  });
};
reintentarConexion();

module.exports = mongoose;